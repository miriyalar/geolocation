Testing the example.

Requirements: mongodb, node.js, restify, mongoose
[rest client for testing purpose.]

start the mongdb using "mongdb".
then start node using "node userloc1.js"

open the rest client and send the data using
url: http://localhost:8080/usersData

use "post" to send data (json object) to server.

sample data:
send one at a time right now.
{"user_id":"1","user_name":"user1","longitude":"10.4","latitude":"30.4", "utype":"any"}

{"user_id":"1","user_name":"user1","longitude":"20.4","latitude":"40.4", "utype":"any"}

use "get" to get the data from server.

use "post" to the following url with the data below to get
the matching riders.
[the result set will be 
matching riders at origin
matching riders at destination
matching riders at destination and origin
url: http://localhost:8080/getRiders

{"user_id":"1","user_name":"user1","longitude":"10.4","latitude":"30.4", 
"dest_longitude":"20.4","dest_latitude":"40.4",
"utype":"any"}




