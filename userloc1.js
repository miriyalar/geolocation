var restify = require('restify');

var ip_addr = '127.0.0.1';
var port    =  '8080';
 
var server = restify.createServer({
    name : "GeoRestService"

});
 
server.listen(port ,ip_addr, function(){
    console.log('%s listening at %s ', server.name , server.url);
});

server.use(restify.queryParser());
server.use(restify.bodyParser());
server.use(restify.CORS());

var mongoose = require('mongoose')
db = mongoose.connect('localhost', 'geoLocations1')

Schema = mongoose.Schema;

// define a method to find the closest person

var userLocSchema = new Schema({
  userId: String,
  userName: String,
  geoLoc: {type:[Number], index: '2d'},
  utype : String
});



mongoose.model('user_location', userLocSchema);
var UserLocation = mongoose.model('user_location');

userLocSchema.methods.findClosestFrom = function(cb) 
{
};


function getUsers(req, res , next){
    res.setHeader('Access-Control-Allow-Origin','*');
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    var query = UserLocation.find();
    query.sort({'userId':-1});
    query.exec(function(arr, data) {
      console.log(data);
      res.send(data);
    })
}
 
function postUserPrefs(req , res , next){
    res.setHeader('Access-Control-Allow-Origin','*');
    res.header("Access-Control-Allow-Headers", "X-Requested-With");

    var uLoc = new UserLocation();

    uLoc.userId = req.params.user_id;
    uLoc.userName= req.params.user_name;

    uLoc.geoLoc.push(parseFloat(req.params.longitude));
    uLoc.geoLoc.push(parseFloat(req.params.latitude));

    uLoc.utype = req.params.utype;
    
    uLoc.save(function() {
      res.send(req.body);
    });
}

var userIdMap = {};
function findUser(userId)
{
   return userIdMap[userId];
}

function getRiders(req, res, next) {
    var uLoc = new UserLocation();

    uLoc.userId = req.params.user_id;
    uLoc.userName= req.params.user_name;

    uLoc.geoLoc.push(parseFloat(req.params.longitude));
    uLoc.geoLoc.push(parseFloat(req.params.latitude));

    uLoc.utype = req.params.utype;
    distance = 0.01;

    destCoords = [];
    destCoords.push(parseFloat(req.params.dest_longitude));
    destCoords.push(parseFloat(req.params.dest_latitude));


    var cb = function(err, resultSet) 
             {
                if (!err) {
                  console.log("Users near origin");
                  console.log(resultSet);

                  UserLocation.find({ geoLoc : { $near :destCoords ,$maxDistance:0.01 }, userId: { $ne : uLoc.userId} }, function(err2, destUsers) {
                    
                  if(!err2) { 
                    var jsonObj =[];
                    var item = {};
                    item["origin-users"] = resultSet;
                    jsonObj.push(item);
                    var item2 = {};
                    item2["dest-users"] = destUsers;
                    jsonObj.push(item2);
                    var item3 = {}
                    var i = null;
                    userIdMap ={};
                    for(i=0;i<resultSet.length;i++)
                    {
                       userIdMap[resultSet[i].userId] = resultSet[i];
                    }
                    var jsonObj2 =[];
                    for(i=0;i<destUsers.length;i++)
                    {
                      var itemx = findUser(destUsers[i].userId);  
                      if(itemx)
                      {
                        jsonObj2.push(itemx);
                      }
                    }
                    item3["to-from-users"] = jsonObj2;
                    jsonObj.push(item3);

                    res.json(jsonObj);
                    }
                    else {
                    throw err2;
                    }
                  });
                } 
                 else {
                    throw err;
                  } 
             };
    UserLocation.find({ geoLoc : { $near :uLoc.geoLoc ,$maxDistance:0.01 },
    userId: { $ne : uLoc.userId} }, cb);
};


var PATH = '/usersData'
server.get({path : PATH , version : '0.0.1'} , getUsers);
server.post({path : PATH , version: '0.0.1'} ,postUserPrefs);
var PATH2 = '/getRiders'
server.post({path : PATH2 , version: '0.0.1'} ,getRiders);

